#include "QYTMusic.h"


QYTMusic* QYTMusic::instance = nullptr;

const QString QYTMusic::ua = QStringLiteral("com.google.ios.youtubemusic/5.04.1 iSL/3.4 iPhone/15.4.1 hw/iPhone11_2 (gzip)");

QYTMusic::QYTMusic(QObject *parent, QString  refreshToken, QString  deviceId) :
    QObject(parent),
    refreshToken(std::move(refreshToken)),
    deviceId(std::move(deviceId))
    {

}

bool QYTMusic::auth() {
    auto *nam = new QNetworkAccessManager(this);
    auto req = QNetworkRequest(QUrl(QStringLiteral("https://www.googleapis.com/oauth2/v4/token")));
    req.setHeader(QNetworkRequest::ContentTypeHeader, QStringLiteral("application/x-www-form-urlencoded"));
    req.setHeader(QNetworkRequest::UserAgentHeader, ua);

    auto data = QUrlQuery({
      {QStringLiteral("client_id"), QStringLiteral("936475272427.apps.googleusercontent.com")},
      {QStringLiteral("grant_type"), QStringLiteral("refresh_token")},
      {QStringLiteral("refresh_token"), refreshToken},
    });

    QEventLoop loop;
    auto *reply = nam->post(req, data.query().toLocal8Bit());
    connect(reply, &QNetworkReply::finished, &loop, &QEventLoop::quit);
    loop.exec();
    auto resp = reply->readAll();
    reply->deleteLater();

    auto respData = QJsonDocument::fromJson(resp);
    if (respData[QStringLiteral("access_token")].isNull()){
        return false;
    }
    auto token = respData[QStringLiteral("access_token")].toString();

    req = QNetworkRequest(QUrl(QStringLiteral("https://oauthaccountmanager.googleapis.com/v1/issuetoken")));
    req.setHeader(QNetworkRequest::ContentTypeHeader, QStringLiteral("application/x-www-form-urlencoded"));
    req.setHeader(QNetworkRequest::UserAgentHeader, ua);
    req.setRawHeader(QByteArray("Authorization"), QString(QStringLiteral("Bearer %1")).arg(token).toLocal8Bit());

    data = QUrlQuery({
         {QStringLiteral("app_id"), QStringLiteral("com.google.ios.youtubemusic")},
         {QStringLiteral("client_id"), QStringLiteral("755973059757-iigsfdoqt2c4qm209soqp2dlrh33almr.apps.googleusercontent.com")},
         {QStringLiteral("device_id"), deviceId},
         {QStringLiteral("hl"), QStringLiteral("ru-UA")},
         {QStringLiteral("lib_ver"), QStringLiteral("3.4")},
         {QStringLiteral("response_type"), QStringLiteral("token")},
         {QStringLiteral("scope"), QStringLiteral("https://www.googleapis.com/auth/youtube https://www.googleapis.com/auth/youtube.force-ssl https://www.googleapis.com/auth/identity.lateimpersonation https://www.googleapis.com/auth/supportcontent https://www.googleapis.com/auth/account_settings_mobile")},
    });

    reply = nam->post(req, data.query().toLocal8Bit());
    connect(reply, &QNetworkReply::finished, &loop, &QEventLoop::quit);
    loop.exec();
    resp = reply->readAll();
    reply->deleteLater();

    respData = QJsonDocument::fromJson(resp);
    if (respData[QStringLiteral("token")].isNull()){
        return false;
    }

    accessToken = respData[QStringLiteral("token")].toString();
    nam->deleteLater();
    return true;
}

QJsonObject QYTMusic::makeRequest(const QString& endpoint, QJsonObject data) {
    auto url = QString(QStringLiteral("https://youtubei.googleapis.com/youtubei/v1/%1?key=AIzaSyBAETezhkwP0ZWA02RsqT1zu78Fpt0bC_s")).arg(endpoint);

    auto payload = QJsonObject{
        {QStringLiteral("context"), QJsonObject{
            {QStringLiteral("client"), QJsonObject{
                {QStringLiteral("clientName"), QStringLiteral("IOS_MUSIC")},
                {QStringLiteral("clientVersion"), QStringLiteral("5.04.1")}
            }}
        }}
    };
    for (const auto& key : data.keys()){
        payload[key] = data[key].toString();
    }

    auto *nam = new QNetworkAccessManager(this);
    auto req = QNetworkRequest(QUrl(url));
    req.setHeader(QNetworkRequest::ContentTypeHeader, QStringLiteral("application/json"));
    req.setHeader(QNetworkRequest::UserAgentHeader, ua);
    req.setRawHeader(QByteArray("Accept"), QByteArray("*/*"));
    req.setRawHeader(QByteArray("Authorization"), QString(QStringLiteral("Bearer %1")).arg(accessToken).toLocal8Bit());

    QEventLoop loop;
    auto *reply = nam->post(req, QJsonDocument(payload).toJson());
    connect(reply, &QNetworkReply::finished, &loop, &QEventLoop::quit);
    loop.exec();
    auto resp = reply->readAll();
    reply->deleteLater();
    nam->deleteLater();

    auto respJson = QJsonDocument::fromJson(resp).object();
    return respJson;
}


QYTMusic::~QYTMusic() {}

QList<QYTMusic::TrackInfo> QYTMusic::getPlaylist(const QString &playlistId) {
    QList<QYTMusic::TrackInfo> result;

    auto resp = makeRequest(QStringLiteral("browse"), QJsonObject{{QStringLiteral("browseId"), playlistId}});

    auto parser = [&resp, &result](){
        //    contents.singleColumnBrowseResultsRenderer.tabs[0].tabRenderer.content.sectionListRenderer.contents[0].musicPlaylistShelfRenderer.contents
        //    contents.singleColumnBrowseResultsRenderer.tabs[0].tabRenderer.content.sectionListRenderer.contents[0].musicShelfRenderer.contents
        auto contentElements = resp[QStringLiteral("contents")].toObject()
                [QStringLiteral("singleColumnBrowseResultsRenderer")].toObject()
                [QStringLiteral("tabs")].toArray()[0].toObject()
                [QStringLiteral("tabRenderer")].toObject()
                [QStringLiteral("content")].toObject()
                [QStringLiteral("sectionListRenderer")].toObject()
                [QStringLiteral("contents")].toArray()[0].toObject()
                [QStringLiteral("musicPlaylistShelfRenderer")].toObject()
                [QStringLiteral("contents")].toArray();
        if (contentElements.isEmpty()){
            contentElements = resp[QStringLiteral("contents")].toObject()
                [QStringLiteral("singleColumnBrowseResultsRenderer")].toObject()
                [QStringLiteral("tabs")].toArray()[0].toObject()
                [QStringLiteral("tabRenderer")].toObject()
                [QStringLiteral("content")].toObject()
                [QStringLiteral("sectionListRenderer")].toObject()
                [QStringLiteral("contents")].toArray()[0].toObject()
                [QStringLiteral("musicShelfRenderer")].toObject()
                [QStringLiteral("contents")].toArray();
        }

        if (contentElements.isEmpty()){
            // continuationContents.musicPlaylistShelfContinuation.contents
            contentElements = resp[QStringLiteral("continuationContents")].toObject()
                    [QStringLiteral("musicPlaylistShelfContinuation")].toObject()
                    [QStringLiteral("contents")].toArray();
        }
        if (contentElements.isEmpty()){
            // continuationContents.musicPlaylistShelfContinuation.contents
            contentElements = resp[QStringLiteral("continuationContents")].toObject()
                    [QStringLiteral("musicShelfContinuation")].toObject()
                    [QStringLiteral("contents")].toArray();
        }

        for (const auto& elem : contentElements){

            // .musicTwoColumnItemRenderer.navigationEndpoint.watchEndpoint.videoId
            const auto& videoId = elem.toObject()
                [QStringLiteral("musicTwoColumnItemRenderer")].toObject()
                [QStringLiteral("navigationEndpoint")].toObject()
                [QStringLiteral("watchEndpoint")].toObject()
                [QStringLiteral("videoId")].toString();
            if (videoId.isEmpty()){
                continue;
            }

            // .musicTwoColumnItemRenderer.title.runs[0].text
            const auto& title = elem.toObject()
                    [QStringLiteral("musicTwoColumnItemRenderer")].toObject()
                    [QStringLiteral("title")].toObject()
                    [QStringLiteral("runs")].toArray()[0].toObject()
                    [QStringLiteral("text")].toString();

            // .musicTwoColumnItemRenderer.subtitle.runs[0].text
            const auto& artist = elem.toObject()
                    [QStringLiteral("musicTwoColumnItemRenderer")].toObject()
                    [QStringLiteral("subtitle")].toObject()
                    [QStringLiteral("runs")].toArray()[0].toObject()
                    [QStringLiteral("text")].toString();

            // .musicTwoColumnItemRenderer.thumbnail.musicThumbnailRenderer.thumbnail.thumbnails[0].url
            const auto& thumbnail = elem.toObject()
                [QStringLiteral("musicTwoColumnItemRenderer")].toObject()
                [QStringLiteral("thumbnail")].toObject()
                [QStringLiteral("musicThumbnailRenderer")].toObject()
                [QStringLiteral("thumbnail")].toObject()
                [QStringLiteral("thumbnails")].toArray()[0].toObject()
                [QStringLiteral("url")].toString();

            result << TrackInfo{videoId, title, artist, thumbnail, QLatin1String(""), 0};
        }
    };

    parser();

    // contents.singleColumnBrowseResultsRenderer.tabs[0].tabRenderer.content.sectionListRenderer.contents[0].musicPlaylistShelfRenderer.continuations[1].nextContinuationData.continuation
    auto continuation = resp[QStringLiteral("contents")].toObject()
            [QStringLiteral("singleColumnBrowseResultsRenderer")].toObject()
            [QStringLiteral("tabs")].toArray()[0].toObject()
            [QStringLiteral("tabRenderer")].toObject()
            [QStringLiteral("content")].toObject()
            [QStringLiteral("sectionListRenderer")].toObject()
            [QStringLiteral("contents")].toArray()[0].toObject()
            [QStringLiteral("musicPlaylistShelfRenderer")].toObject()
            [QStringLiteral("continuations")].toArray()[1].toObject()
            [QStringLiteral("nextContinuationData")].toObject()
            [QStringLiteral("continuation")].toString();

    while (!continuation.isEmpty()){
        resp = makeRequest(QStringLiteral("browse"), QJsonObject{{QStringLiteral("continuation"), QString::fromLocal8Bit(QByteArray::fromPercentEncoding(continuation.toLocal8Bit()))}});
        continuation = resp[QStringLiteral("contents")].toObject()
            [QStringLiteral("continuationContents")].toObject()
            [QStringLiteral("musicPlaylistShelfRenderer")].toObject()
            [QStringLiteral("continuations")].toArray()[1].toObject()
            [QStringLiteral("nextContinuationData")].toObject()
            [QStringLiteral("continuation")].toString();

        parser();
    }

    return result;


}

QYTMusic::TrackInfo QYTMusic::getFullTrackInfo(const QString &videoId) {
    const auto& resp = makeRequest(QStringLiteral("player"), QJsonObject{{QStringLiteral("videoId"), videoId}});

    // streamingData.adaptiveFormats
    auto adaptiveFormats = resp[QStringLiteral("streamingData")].toObject()
            [QStringLiteral("adaptiveFormats")].toArray().toVariantList();

    auto thumbnailObjects = resp[QStringLiteral("videoDetails")].toObject()
            [QStringLiteral("thumbnail")].toObject()
            [QStringLiteral("thumbnails")].toArray();

    std::sort(thumbnailObjects.begin(), thumbnailObjects.end(), [](auto x, auto y){
        return y.toObject()[QStringLiteral("width")].toInt() * y.toObject()[QStringLiteral("height")].toInt() < x.toObject()[QStringLiteral("width")].toInt() * x.toObject()[QStringLiteral("height")].toInt();
    });

    auto result = QYTMusic::TrackInfo{
        videoId,
        resp[QStringLiteral("videoDetails")].toObject()[QStringLiteral("title")].toString(),
        resp[QStringLiteral("videoDetails")].toObject()[QStringLiteral("author")].toString(),
        thumbnailObjects[0].toObject()[QStringLiteral("url")].toString(),
        QLatin1String(""),
        0,
        resp[QStringLiteral("videoDetails")].toObject()[QStringLiteral("lengthSeconds")].toString().toInt()
        };

    auto qualityRate = 0;

    for (const auto& el: adaptiveFormats){
        if (el.toMap()[QStringLiteral("mimeType")].toString().contains(QStringLiteral("audio/mp4"))){
            auto currentQR = el.toMap()[QStringLiteral("itag")].toInt();
            if (currentQR > qualityRate){
                qualityRate = currentQR;
                result.url = el.toMap()[QStringLiteral("url")].toString() + QStringLiteral("&__videoId=") + videoId;
                result.size = el.toMap()[QStringLiteral("contentLength")].toInt();
                result.sampleRate = el.toMap()[QStringLiteral("audioSampleRate")].toInt();
                result.channels = el.toMap()[QStringLiteral("audioChannels")].toInt();
                result.bitrate = el.toMap()[QStringLiteral("bitrate")].toInt();
            }
        }
    }

    database.insert(QUrl::fromUserInput(result.url), result);

    return result;
}

QYTMusic::GeneralEntityInfo QYTMusic::generalEntityList(const QString &browseId, const QString& params) {

    QYTMusic::GeneralEntityInfo result;

    if (browseId.startsWith(QStringLiteral("VLPL"))){
        result.tracks = getPlaylist(browseId);
        return result;
    }

    auto payload = QJsonObject{{QStringLiteral("browseId"), browseId}};
    if (!params.isEmpty()){
        payload.insert(QStringLiteral("params"), params);
    }

    const auto& resp = makeRequest(QStringLiteral("browse"), payload);

    auto contentElements = resp[QStringLiteral("contents")].toObject()
        [QStringLiteral("singleColumnBrowseResultsRenderer")].toObject()
        [QStringLiteral("tabs")].toArray()[0].toObject()
        [QStringLiteral("tabRenderer")].toObject()
        [QStringLiteral("content")].toObject()
        [QStringLiteral("sectionListRenderer")].toObject()
        [QStringLiteral("contents")].toArray();

    for (const auto& contentElement : contentElements){
        if (contentElement.toObject().contains(QStringLiteral("musicCarouselShelfRenderer"))){
            const auto& name = contentElement.toObject()[QStringLiteral("musicCarouselShelfRenderer")].toObject()
                    [QStringLiteral("header")].toObject()
                    [QStringLiteral("musicCarouselShelfBasicHeaderRenderer")].toObject()
                    [QStringLiteral("title")].toObject()
                    [QStringLiteral("runs")].toArray()[0].toObject()
                    [QStringLiteral("text")].toString();

            const auto& id = contentElement.toObject()[QStringLiteral("musicCarouselShelfRenderer")].toObject()
                [QStringLiteral("header")].toObject()
                [QStringLiteral("musicCarouselShelfBasicHeaderRenderer")].toObject()
                [QStringLiteral("navigationEndpoint")].toObject()
                [QStringLiteral("browseEndpoint")].toObject()
                [QStringLiteral("browseId")].toString();

            const auto& params = contentElement.toObject()[QStringLiteral("musicCarouselShelfRenderer")].toObject()
                [QStringLiteral("header")].toObject()
                [QStringLiteral("musicCarouselShelfBasicHeaderRenderer")].toObject()
                [QStringLiteral("navigationEndpoint")].toObject()
                [QStringLiteral("browseEndpoint")].toObject()
                [QStringLiteral("params")].toString();

            if (!id.isEmpty())
                result.playlists.push_back({id, name, QLatin1String(""), params});


            const auto& musicContents = contentElement.toObject()[QStringLiteral("musicCarouselShelfRenderer")].toObject()
                    [QStringLiteral("contents")].toArray();

            for (const auto& musicContent: musicContents){


                const auto& musicTitle = musicContent.toObject()[QStringLiteral("musicTwoRowItemRenderer")].toObject()
                        [QStringLiteral("title")].toObject()
                        [QStringLiteral("runs")].toArray()[0].toObject()
                        [QStringLiteral("text")].toString();

                const auto& musicId = musicContent.toObject()[QStringLiteral("musicTwoRowItemRenderer")].toObject()
                        [QStringLiteral("navigationEndpoint")].toObject()
                        [QStringLiteral("browseEndpoint")].toObject()
                        [QStringLiteral("browseId")].toString();

                if (musicId.isEmpty()){
                    continue;
                }

                auto thumbnailObjects = resp[QStringLiteral("musicTwoRowItemRenderer")].toObject()
                        [QStringLiteral("thumbnailRenderer")].toObject()
                        [QStringLiteral("musicThumbnailRenderer")].toObject()
                        [QStringLiteral("thumbnail")].toObject()
                        [QStringLiteral("thumbnails")].toArray();

                std::sort(thumbnailObjects.begin(), thumbnailObjects.end(), [](auto x, auto y){
                    return y.toObject()[QStringLiteral("width")].toInt() * y.toObject()[QStringLiteral("height")].toInt() < x.toObject()[QStringLiteral("width")].toInt() * x.toObject()[QStringLiteral("height")].toInt();
                });

                const auto& musicThumbnail = thumbnailObjects[0].toObject()[QStringLiteral("url")].toString();

                result.playlists.push_back({musicId, musicTitle, musicThumbnail, QLatin1String()});

            }

        }
        if (contentElement.toObject().contains(QStringLiteral("musicShelfRenderer"))){
            for (const auto& contentSubElement : contentElement.toObject()
            [QStringLiteral("musicShelfRenderer")].toObject()
            [QStringLiteral("contents")].toArray()) {

                const auto& title = contentSubElement.toObject()
                    [QStringLiteral("musicTwoColumnItemRenderer")].toObject()
                    [QStringLiteral("title")].toObject()
                    [QStringLiteral("runs")].toArray()[0].toObject()
                    [QStringLiteral("text")].toString();



                const auto& thumbnail = contentSubElement.toObject()
                    [QStringLiteral("musicTwoColumnItemRenderer")].toObject()
                    [QStringLiteral("thumbnail")].toObject()
                    [QStringLiteral("musicThumbnailRenderer")].toObject()
                    [QStringLiteral("thumbnail")].toObject()
                    [QStringLiteral("thumbnails")].toArray().last()
                    [QStringLiteral("url")].toString();

                const auto& navigationEndpoint = contentSubElement.toObject()
                    [QStringLiteral("musicTwoColumnItemRenderer")].toObject()
                    [QStringLiteral("navigationEndpoint")].toObject();

                if (navigationEndpoint.contains(QStringLiteral("browseEndpoint"))){
                    const auto& id = navigationEndpoint[QStringLiteral("browseEndpoint")].toObject()
                            [QStringLiteral("browseId")].toString();

                    if (id.isEmpty()){
                        continue;
                    }

                    const auto& params = navigationEndpoint[QStringLiteral("browseEndpoint")].toObject()
                            [QStringLiteral("params")].toString();
                    result.playlists.push_back({id, title, thumbnail, params});
                }
                else {
                    const auto& id = contentSubElement.toObject()
                        [QStringLiteral("musicTwoColumnItemRenderer")].toObject()
                        [QStringLiteral("navigationEndpoint")].toObject()
                        [QStringLiteral("watchEndpoint")].toObject()
                        [QStringLiteral("videoId")].toString();

                    if (id.isEmpty()){
                        continue;
                    }

                    const auto& artist = contentSubElement.toObject()
                        [QStringLiteral("musicTwoColumnItemRenderer")].toObject()
                        [QStringLiteral("subtitle")].toObject()
                        [QStringLiteral("runs")].toArray()[0].toObject()
                        [QStringLiteral("text")].toString();

                    result.tracks.push_back({id, title, artist, thumbnail, QLatin1String(""), 0});
                }

            }
        }
        if (contentElement.toObject().contains(QStringLiteral("musicSpotlightShelfRenderer"))){
            for (const auto& contentSubElement : contentElement.toObject()
                [QStringLiteral("musicSpotlightShelfRenderer")].toObject()
                [QStringLiteral("contents")].toArray()){
                    const auto& title = contentSubElement.toObject()
                        [QStringLiteral("musicSpotlightItemRenderer")].toObject()
                        [QStringLiteral("title")].toObject()
                        [QStringLiteral("runs")].toArray()[0].toObject()
                        [QStringLiteral("text")].toString();

                    const auto& thumbnail = contentSubElement.toObject()
                        [QStringLiteral("musicSpotlightItemRenderer")].toObject()
                        [QStringLiteral("thumbnailRenderer")].toObject()
                        [QStringLiteral("musicThumbnailRenderer")].toObject()
                        [QStringLiteral("thumbnail")].toObject()
                        [QStringLiteral("thumbnails")].toArray().last()
                        [QStringLiteral("url")].toString();

                    const auto& navigationEndpoint = contentSubElement.toObject()
                        [QStringLiteral("musicSpotlightItemRenderer")].toObject()
                        [QStringLiteral("navigationEndpoint")].toObject();
//                        [QStringLiteral("browserEndpoint")].toObject();

                    if (navigationEndpoint.contains(QStringLiteral("browseEndpoint"))){
                        const auto& id = navigationEndpoint[QStringLiteral("browseEndpoint")].toObject()
                                [QStringLiteral("browseId")].toString();
                        if (id.isEmpty()){
                            continue;
                        }

                        const auto& params = navigationEndpoint[QStringLiteral("browseEndpoint")].toObject()
                                [QStringLiteral("params")].toString();

                        result.playlists.push_back({id, title, thumbnail, params});
                    }
                    else {
                        const auto& id = navigationEndpoint[QStringLiteral("watchEndpoint")].toObject()
                            [QStringLiteral("videoId")].toString();

                        if (id.isEmpty()){
                            continue;
                        }
                        const auto& artist = contentSubElement.toObject()
                            [QStringLiteral("musicSpotlightItemRenderer")].toObject()
                            [QStringLiteral("subtitle")].toObject()
                            [QStringLiteral("runs")].toArray()[0].toObject()
                            [QStringLiteral("text")].toString();

                        result.tracks.push_back({id, title, artist, thumbnail, QLatin1String(""), 0});
                    }

            }
        }
        if (contentElement.toObject().contains(QStringLiteral("gridRenderer"))){
            for (const auto& contentSubElement : contentElement.toObject()
                [QStringLiteral("gridRenderer")].toObject()
                [QStringLiteral("items")].toArray()){


                if (contentSubElement.toObject().contains(QStringLiteral("musicNavigationButtonRenderer"))){
                    const auto& title = contentSubElement.toObject()
                            [QStringLiteral("musicNavigationButtonRenderer")].toObject()
                            [QStringLiteral("buttonText")].toObject()
                            [QStringLiteral("runs")].toArray()[0].toObject()
                            [QStringLiteral("text")].toString();

                    const auto& browseId = contentSubElement.toObject()
                            [QStringLiteral("musicNavigationButtonRenderer")].toObject()
                            [QStringLiteral("clickCommand")].toObject()
                            [QStringLiteral("browseEndpoint")].toObject()
                            [QStringLiteral("browseId")].toString();

                    const auto& params = contentSubElement.toObject()
                            [QStringLiteral("musicNavigationButtonRenderer")].toObject()
                            [QStringLiteral("clickCommand")].toObject()
                            [QStringLiteral("browseEndpoint")].toObject()
                            [QStringLiteral("params")].toString();

                    result.playlists << QYTMusic::PlaylistInfo{browseId, title, QLatin1String(), params};
                } else {

                    const auto& title = contentSubElement.toObject()[QStringLiteral("musicTwoRowItemRenderer")].toObject()
                            [QStringLiteral("title")].toObject()
                            [QStringLiteral("runs")].toArray()[0].toObject()
                            [QStringLiteral("text")].toString();

                    const auto& thumbnail = contentSubElement.toObject()[QStringLiteral("musicTwoRowItemRenderer")].toObject()
                            [QStringLiteral("thumbnailRenderer")].toObject()
                            [QStringLiteral("musicThumbnailRenderer")].toObject()
                            [QStringLiteral("thumbnail")].toObject()
                            [QStringLiteral("thumbnails")].toArray().last().toObject()
                            [QStringLiteral("url")].toString();

                    const auto& navigationEndpoint = contentSubElement.toObject()[QStringLiteral("musicTwoRowItemRenderer")].toObject()
                            [QStringLiteral("navigationEndpoint")].toObject();

                    if (navigationEndpoint.contains(QStringLiteral("browseEndpoint"))){
                        const auto& id = navigationEndpoint[QStringLiteral("browseEndpoint")].toObject()
                                [QStringLiteral("browseId")].toString();
                        if (id.isEmpty()){
                            continue;
                        }

                        const auto& params = navigationEndpoint[QStringLiteral("browseEndpoint")].toObject()
                                [QStringLiteral("params")].toString();

                        result.playlists.push_back({id, title, thumbnail, params});
                    }
                    else {
                        const auto& id = navigationEndpoint[QStringLiteral("browseEndpoint")].toObject()
                        [QStringLiteral("videoId")].toString();
                        if (id.isEmpty()){
                            continue;
                        }

                        const auto& artist = contentSubElement.toObject()[QStringLiteral("musicTwoRowItemRenderer")].toObject()
                            [QStringLiteral("subtitle")].toObject()
                            [QStringLiteral("runs")].toArray()[0].toObject()
                            [QStringLiteral("text")].toString();

                        result.tracks.push_back({id, title, artist, thumbnail, QLatin1String(""), 0});
                    }
                }
            }
        }
        if (contentElement.toObject().contains(QStringLiteral("musicPlaylistShelfRenderer"))){
            for (const auto& contentSubElement: contentElement.toObject()[QStringLiteral("musicPlaylistShelfRenderer")].toObject()
            [QStringLiteral("contents")].toArray()){

                const auto& id = contentSubElement.toObject()
                    [QStringLiteral("musicTwoColumnItemRenderer")].toObject()
                    [QStringLiteral("navigationEndpoint")].toObject()
                    [QStringLiteral("watchEndpoint")].toObject()
                    [QStringLiteral("videoId")].toString();

                if (id.isEmpty()){
                    continue;
                }

                const auto& title = contentSubElement.toObject()
                    [QStringLiteral("musicTwoColumnItemRenderer")].toObject()
                    [QStringLiteral("title")].toObject()
                    [QStringLiteral("runs")].toArray()[0].toObject()
                    [QStringLiteral("text")].toString();

                const auto& artist = contentSubElement.toObject()
                    [QStringLiteral("musicTwoColumnItemRenderer")].toObject()
                    [QStringLiteral("subtitle")].toObject()
                    [QStringLiteral("runs")].toArray()[0].toObject()
                    [QStringLiteral("text")].toString();

                const auto& thumbnail = contentSubElement.toObject()
                    [QStringLiteral("musicTwoColumnItemRenderer")].toObject()
                    [QStringLiteral("thumbnail")].toObject()
                    [QStringLiteral("musicThumbnailRenderer")].toObject()
                    [QStringLiteral("thumbnail")].toObject()
                    [QStringLiteral("thumbnails")].toArray().last().toObject()
                    [QStringLiteral("url")].toString();

                result.tracks.push_back({id, title, artist, thumbnail, QLatin1String(""), 0});
            }
        }

    }

    return result;
}

QYTMusic *QYTMusic::getInstance()
{
    if (instance == nullptr){
        instance = new QYTMusic(
                    nullptr,
                    Elisa::ElisaConfiguration::self()->youtubeMusicRefreshToken(),
                    Elisa::ElisaConfiguration::self()->youtubeMusicDeviceId()
        );
        instance->auth();
    }
    return instance;
}
