#ifndef KIO_YTMUSIC_QYTMUSIC_H
#define KIO_YTMUSIC_QYTMUSIC_H


#include <QObject>
#include <utility>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QUrlQuery>
#include <QEventLoop>
#include <algorithm>
#include "elisa_settings.h"

inline void swap(QJsonValueRef v1, QJsonValueRef v2)
{
    QJsonValue temp(v1);
    v1 = QJsonValue(v2);
    v2 = temp;
}

class QYTMusic : QObject {
    Q_OBJECT

    QString refreshToken;
    QString deviceId;
    QString accessToken;
    static const QString ua;
    QJsonObject makeRequest(const QString& endpoint, QJsonObject data);

    static QYTMusic* instance;

public:
    struct PlaylistInfo {
        QString id;
        QString name;
        QString thumbnail;
        QString params = QStringLiteral("");
        ~PlaylistInfo() = default;
    };


    struct TrackInfo {
        QString id;
        QString title;
        QString artist;
        QString thumbnail;
        QString url;
        int size;
        int length = 0;
        int sampleRate = 0;
        int channels = 0;
        int bitrate = 0;
        ~TrackInfo() = default;
    };

    struct GeneralEntityInfo {
        QList<PlaylistInfo> playlists;
        QList<TrackInfo> tracks;
    };

    QYTMusic(QObject *parent, QString  refreshToken, QString  deviceId);
    bool auth();

    QList<TrackInfo> getPlaylist(const QString& playlistId);
    TrackInfo getFullTrackInfo(const QString& videoId);
    GeneralEntityInfo generalEntityList(const QString& browseId, const QString& params=QLatin1String(""));

    static QYTMusic* getInstance();

    ~QYTMusic() override;

    QHash<QUrl, TrackInfo> database;

};


#endif //KIO_YTMUSIC_QYTMUSIC_H
