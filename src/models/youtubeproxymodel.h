#ifndef YOUTUBEPROXYMODEL_H
#define YOUTUBEPROXYMODEL_H

#include "abstractmediaproxymodel.h"

#include <QSortFilterProxyModel>
#include <QObject>
#include <mediaplaylistproxymodel.h>
#include <qytmusic/QYTMusic.h>

#include <QFileInfo>

class YoutubeProxyModel : public AbstractMediaProxyModel
{
    Q_OBJECT


public:
    explicit YoutubeProxyModel(QObject *parent = nullptr);
    ~YoutubeProxyModel() override;

public Q_SLOTS:
    void enqueue(const DataTypes::MusicDataType &newEntry,
                 const QString &newEntryTitle,
                 ElisaUtils::PlayListEnqueueMode enqueueMode,
                 ElisaUtils::PlayListEnqueueTriggerPlay triggerPlay);

    void replaceAndPlayOfPlayList(const QModelIndex &rootIndex);
    void enqueueToPlayList(const QModelIndex &rootIndex);

protected:
    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const override;

private:
    void genericEnqueueToPlayList(const QModelIndex &rootIndex,
                                  ElisaUtils::PlayListEnqueueMode enqueueMode,
                                  ElisaUtils::PlayListEnqueueTriggerPlay triggerPlay);

};

#endif // YOUTUBEPROXYMODEL_H
