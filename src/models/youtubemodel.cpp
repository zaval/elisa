#include "youtubemodel.h"

#include <QFileInfo>

YoutubeModel::YoutubeModel(QObject *parent)
    : QAbstractListModel{parent}
{

}

QHash<int, QByteArray> YoutubeModel::roleNames() const
{
    auto roles = QAbstractListModel::roleNames();

    roles[static_cast<int>(DataTypes::ColumnsRoles::TitleRole)] = "title";
    roles[static_cast<int>(DataTypes::ColumnsRoles::SecondaryTextRole)] = "secondaryText";
    roles[static_cast<int>(DataTypes::ColumnsRoles::ImageUrlRole)] = "imageUrl";
    roles[static_cast<int>(DataTypes::ColumnsRoles::DatabaseIdRole)] = "databaseId";
    roles[static_cast<int>(DataTypes::ColumnsRoles::ElementTypeRole)] = "dataType";
    roles[static_cast<int>(DataTypes::ColumnsRoles::ResourceRole)] = "url";

    roles[static_cast<int>(DataTypes::ColumnsRoles::ArtistRole)] = "artist";
    roles[static_cast<int>(DataTypes::ColumnsRoles::AllArtistsRole)] = "allArtists";
    roles[static_cast<int>(DataTypes::ColumnsRoles::HighestTrackRating)] = "highestTrackRating";
    roles[static_cast<int>(DataTypes::ColumnsRoles::GenreRole)] = "genre";

    roles[static_cast<int>(DataTypes::ColumnsRoles::AlbumRole)] = "album";
    roles[static_cast<int>(DataTypes::ColumnsRoles::AlbumArtistRole)] = "albumArtist";
    roles[static_cast<int>(DataTypes::ColumnsRoles::DurationRole)] = "duration";
    roles[static_cast<int>(DataTypes::ColumnsRoles::TrackNumberRole)] = "trackNumber";
    roles[static_cast<int>(DataTypes::ColumnsRoles::DiscNumberRole)] = "discNumber";
    roles[static_cast<int>(DataTypes::ColumnsRoles::RatingRole)] = "rating";
    roles[static_cast<int>(DataTypes::ColumnsRoles::IsSingleDiscAlbumRole)] = "isSingleDiscAlbum";
    roles[static_cast<int>(DataTypes::ColumnsRoles::FullDataRole)] = "fullData";
    roles[static_cast<int>(DataTypes::ColumnsRoles::HasChildrenRole)] = "hasChildren";

    roles[static_cast<int>(DataTypes::ColumnsRoles::IsDirectoryRole)] = "isDirectory";
    roles[static_cast<int>(DataTypes::ColumnsRoles::IsPlayListRole)] = "isPlaylist";

    return roles;
}

int YoutubeModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)

    return innerItems.count();

}

QVariant YoutubeModel::data(const QModelIndex &index, int role) const
{

    auto result = QVariant();

    auto isPlaylist = innerItems[index.row()].type == ItemTypeType::PLAYLIST;
    auto newUrl = QUrl(url);
    if (newUrl.path() == QStringLiteral("/") || newUrl.path() == QLatin1String()){
        newUrl.setPath(isPlaylist ? innerItems[index.row()].playlist.id  : innerItems[index.row()].track.id);
    }
    else {
        newUrl.setPath(newUrl.path() + QStringLiteral("/") + (isPlaylist ? innerItems[index.row()].playlist.id  : innerItems[index.row()].track.id));
    }

    if (isPlaylist){
        newUrl.setQuery(QStringLiteral("params=%1").arg(innerItems[index.row()].playlist.params));
    }


    switch (role) {
    case DataTypes::ColumnsRoles::ResourceRole:

        result = newUrl;
        break;
    case DataTypes::ColumnsRoles::ImageUrlRole:
           result = isPlaylist ? QUrl(innerItems[index.row()].playlist.thumbnail) : QUrl(innerItems[index.row()].track.thumbnail);
        break;
    case DataTypes::ColumnsRoles::IsDirectoryRole:
    case DataTypes::ColumnsRoles::HasChildrenRole:
        result = isPlaylist;
        break;
    case DataTypes::ColumnsRoles::FullDataRole:

        if (isPlaylist)
            result = QVariant::fromValue(DataTypes::MusicDataType{
                                                              {DataTypes::ColumnsRoles::FilePathRole, newUrl},
                                                              {DataTypes::ColumnsRoles::ElementTypeRole, ElisaUtils::Container},
                                                              {DataTypes::TitleRole, innerItems[index.row()].playlist.name},
                                                              {DataTypes::ImageUrlRole, QUrl(innerItems[index.row()].playlist.thumbnail)}});
        else
            result = QVariant::fromValue(DataTypes::MusicDataType{
                                                                  {DataTypes::ColumnsRoles::ResourceRole, newUrl},
                                                                  {DataTypes::ColumnsRoles::ElementTypeRole, ElisaUtils::Track},
                                                                  {DataTypes::TitleRole, innerItems[index.row()].track.title},
                                                                  {DataTypes::ImageUrlRole, QUrl(innerItems[index.row()].track.thumbnail)}});
        break;
    case DataTypes::ColumnsRoles::FilePathRole:
        result = newUrl;
        break;
    case DataTypes::ColumnsRoles::IsPlayListRole:
        result = isPlaylist;
        break;
    case DataTypes::ColumnsRoles::ElementTypeRole:
        result = isPlaylist ? ElisaUtils::Container : ElisaUtils::FileName;
        break;
    case Qt::DisplayRole:
        result = isPlaylist ? innerItems[index.row()].playlist.name : innerItems[index.row()].track.title;
        break;
    default:
        break;
    }

    return result;
}

bool YoutubeModel::isBusy() const
{
    return mIsBusy;
}

void YoutubeModel::initializeByData(MusicListenersManager *manager, DatabaseInterface *database, ElisaUtils::PlayListEntryType modelType, ElisaUtils::FilterType filter, const DataTypes::DataType &dataFilter)
{
    Q_UNUSED(manager)
    Q_UNUSED(database)
    Q_UNUSED(modelType)
    Q_UNUSED(filter)

    url = dataFilter[DataTypes::FilePathRole].toUrl();

    beginResetModel();
    innerItems.clear();

    if (url.toString() == QStringLiteral("/") || url.toString() == QLatin1String("")){

        innerItems = {
            {
                  ItemTypeType::PLAYLIST,
                  {
                      QStringLiteral("FEmusic_liked_albums"),
                      QStringLiteral("Albums"),
                      QLatin1String("image://icon/folder-book"),
                      QLatin1String("")
                  },
                  {}
              },
            {
                ItemTypeType::PLAYLIST,
                {
                    QStringLiteral("FEmusic_liked_playlists"),
                    QStringLiteral("Playlists"),
                    QLatin1String("image://icon/folder-txt"),
                    QLatin1String("")
                },
                {}
            },
            {
                ItemTypeType::PLAYLIST,
                {
                    QStringLiteral("FEmusic_library_corpus_track_artists"),
                    QStringLiteral("Artists"),
                    QLatin1String("image://icon/folder-public"),
                    QLatin1String("")
                },
                {}
            },
            {
                ItemTypeType::PLAYLIST,
                {
                    QStringLiteral("FEmusic_liked_videos"),
                    QStringLiteral("Tracks"),
                    QLatin1String("image://icon/folder-music"),
                    QLatin1String("")
                },
                {}
            },
            {
                ItemTypeType::PLAYLIST,
                {
                    QStringLiteral("FEmusic_library_corpus_artists"),
                    QStringLiteral("Subscriptions"),
                    QLatin1String("image://icon/favorites"),
                    QLatin1String("")
                },
                {}
            },
            {
                ItemTypeType::PLAYLIST,
                {
                    QStringLiteral("FEmusic_mixed_for_you"),
                    QStringLiteral("Mixed for you"),
                    QLatin1String("image://icon/folder-presentation"),
                    QLatin1String("")
                },
                {}
            },
            {
                ItemTypeType::PLAYLIST,
                {
                    QStringLiteral("FEmusic_moods_and_genres"),
                    QStringLiteral("Moods & Genres"),
                    QLatin1String("image://icon/folder-desktop"),
                    QLatin1String("")
                },
                {}
            },
            {
                ItemTypeType::PLAYLIST,
                {
                    QStringLiteral("FEmusic_charts"),
                    QStringLiteral("Charts"),
                    QLatin1String("image://icon/folder-chart"),
                    QLatin1String("")
                },
                {}
            },
            {
                ItemTypeType::PLAYLIST,
                {
                    QStringLiteral("FEmusic_new_releases"),
                    QStringLiteral("New releases"),
                    QLatin1String("image://icon/folder-extension"),
                    QLatin1String("")
                },
                {}
            },

        };

        endResetModel();

        return;
    }

    mIsBusy = true;
    Q_EMIT isBusyChanged();


    auto yt = QYTMusic::getInstance();
    auto params = url.hasQuery() ? url.query().split(QStringLiteral("="))[1] : QLatin1String();
    auto fi = QFileInfo(url.path()).completeBaseName();
    auto playlists = yt->generalEntityList(fi, params);

    for (auto pl: playlists.playlists){
        if (pl.name == QStringLiteral("Albums")){
            pl.thumbnail = QStringLiteral("image://icon/media-optical");
        }
        innerItems.append({
                              ItemTypeType::PLAYLIST,
                              pl,
                              {}
                          });
    }

    for (auto pl: playlists.tracks){
        pl.url = QStringLiteral("https://ytmusic.me/%1").arg(pl.id);
        if (pl.thumbnail.isEmpty()){
            pl.thumbnail = QStringLiteral("image://icon/audio-x-generic");
        }
        innerItems.append({
                              ItemTypeType::TRACK,
                              {},
                              pl,
                          });

    }

    endResetModel();

    mIsBusy = false;
    Q_EMIT isBusyChanged();
}

void YoutubeModel::initialize(MusicListenersManager *manager, DatabaseInterface *database, ElisaUtils::PlayListEntryType modelType, ElisaUtils::FilterType filter, const QString &genre, const QString &artist, qulonglong databaseId, const QUrl &pathFilter)
{
    Q_UNUSED(manager)
    Q_UNUSED(database)
    Q_UNUSED(modelType)
    Q_UNUSED(filter)
    Q_UNUSED(genre)
    Q_UNUSED(artist)
    Q_UNUSED(databaseId)
    Q_UNUSED(pathFilter)

    qDebug() << "YoutubeModel::initialize";
}

Qt::ItemFlags YoutubeModel::flags(const QModelIndex &index) const
{
    if (!index.isValid()) {
        return Qt::NoItemFlags;
    }

    return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
}

YoutubeModel::~YoutubeModel() = default;
