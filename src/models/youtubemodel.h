#ifndef YOUTUBEMODEL_H
#define YOUTUBEMODEL_H

#include <QAbstractListModel>
#include <databaseinterface.h>
#include <musiclistenersmanager.h>
#include "datatypes.h"
#include <QStringList>
#include <qytmusic/QYTMusic.h>

class ELISALIB_EXPORT YoutubeModel : public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(bool isBusy READ isBusy NOTIFY isBusyChanged)

    bool mIsBusy = false;

    static QStringList folders;

    QUrl url;

    enum ItemTypeType {
        PLAYLIST,
        TRACK
    };

    struct ItemTYpe {
        ItemTypeType type;
        QYTMusic::PlaylistInfo playlist;
        QYTMusic::TrackInfo track;
    };


    QList<ItemTYpe> innerItems;

public:
    explicit YoutubeModel(QObject *parent = nullptr);
    QHash<int, QByteArray> roleNames() const override;

    ~YoutubeModel() override;

    // QAbstractItemModel interface
    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;

    [[nodiscard]] bool isBusy() const;
Q_SIGNALS:

    void isBusyChanged();

public Q_SLOTS:
    void initializeByData(MusicListenersManager *manager, DatabaseInterface *database,
                                            ElisaUtils::PlayListEntryType modelType, ElisaUtils::FilterType filter,
                                            const DataTypes::DataType &dataFilter);

    void initialize(MusicListenersManager *manager, DatabaseInterface *database,
                    ElisaUtils::PlayListEntryType modelType, ElisaUtils::FilterType filter,
                    const QString &genre, const QString &artist, qulonglong databaseId,
                    const QUrl &pathFilter);

    // QAbstractItemModel interface
public:
    Qt::ItemFlags flags(const QModelIndex &index) const override;

};

#endif // YOUTUBEMODEL_H
