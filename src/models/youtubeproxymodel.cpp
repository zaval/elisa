#include "youtubeproxymodel.h"

YoutubeProxyModel::YoutubeProxyModel(QObject *parent)
    : AbstractMediaProxyModel{parent}
{
    setFilterCaseSensitivity(Qt::CaseInsensitive);
    mThreadPool.setMaxThreadCount(1);
}

YoutubeProxyModel::~YoutubeProxyModel()
{

}

void YoutubeProxyModel::enqueue(const DataTypes::MusicDataType &newEntry, const QString &newEntryTitle, ElisaUtils::PlayListEnqueueMode enqueueMode, ElisaUtils::PlayListEnqueueTriggerPlay triggerPlay)
{

    auto yt = QYTMusic::getInstance();
    auto pathRole = newEntry[DataTypes::FilePathRole].toUrl();
    auto fi = QFileInfo(pathRole.path()).completeBaseName();
    if (pathRole.isValid()){
        auto tracks = yt->generalEntityList(fi, pathRole.query().replace(QStringLiteral("params="), QLatin1String()));
        auto allData = DataTypes::EntryDataList{};
        for (const auto &track: tracks.tracks){
            auto trackUrl = QUrl(yt->getFullTrackInfo(track.id).url);

            allData.push_back({{{DataTypes::ColumnsRoles::ResourceRole, QUrl(trackUrl)},
                                {DataTypes::ColumnsRoles::ElementTypeRole, ElisaUtils::FileName},
                                {DataTypes::TitleRole, track.title},
                                {DataTypes::ImageUrlRole, QUrl(track.thumbnail)},
                                {DataTypes::HasEmbeddedCover, true},
                                {DataTypes::ArtistRole, newEntry[DataTypes::ArtistRole].toString()},
                                {DataTypes::AlbumArtistRole, newEntry[DataTypes::ArtistRole].toString()},
                                {DataTypes::AllArtistsRole, newEntry[DataTypes::AllArtistsRole].toString()},
                                {DataTypes::RatingRole, 0},
                                {DataTypes::ElementTypeRole, ElisaUtils::Track},
                                {DataTypes::FileModificationTime, QDateTime::currentDateTime()}},
                                track.title,
                                QUrl(trackUrl)});

        }
        Q_EMIT entriesToEnqueue(allData, enqueueMode, triggerPlay);
        return;
    }

    auto trackId = QFileInfo(newEntry[DataTypes::ResourceRole].toUrl().path()).completeBaseName();
    auto trackUrl = QUrl(yt->getFullTrackInfo(trackId).url);
    auto allData = DataTypes::EntryDataList{};

    allData.push_back({{{DataTypes::ColumnsRoles::ResourceRole, QUrl(trackUrl)},
                        {DataTypes::ColumnsRoles::ElementTypeRole, ElisaUtils::FileName},
                        {DataTypes::TitleRole, newEntry[DataTypes::TitleRole].toString()},
                        {DataTypes::ImageUrlRole, QUrl(newEntry[DataTypes::ImageUrlRole].toUrl())},
                        {DataTypes::HasEmbeddedCover, true},
                        {DataTypes::ArtistRole, newEntry[DataTypes::ArtistRole].toString()},
                        {DataTypes::AlbumArtistRole, newEntry[DataTypes::ArtistRole].toString()},
                        {DataTypes::AllArtistsRole, newEntry[DataTypes::AllArtistsRole].toString()},
                        {DataTypes::RatingRole, 0},
                        {DataTypes::ElementTypeRole, ElisaUtils::Track},
                        {DataTypes::FileModificationTime, QDateTime::currentDateTime()}},
                        newEntryTitle,
                        QUrl(trackUrl)});


    Q_EMIT entriesToEnqueue(allData, enqueueMode, triggerPlay);

}

void YoutubeProxyModel::replaceAndPlayOfPlayList(const QModelIndex &rootIndex)
{
    genericEnqueueToPlayList(rootIndex, ElisaUtils::ReplacePlayList, ElisaUtils::TriggerPlay);
}

void YoutubeProxyModel::enqueueToPlayList(const QModelIndex &rootIndex)
{
    genericEnqueueToPlayList(rootIndex, ElisaUtils::AppendPlayList, ElisaUtils::DoNotTriggerPlay);
}


bool YoutubeProxyModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    Q_UNUSED(source_row);
    Q_UNUSED(source_parent);
    return true;
}

void YoutubeProxyModel::genericEnqueueToPlayList(const QModelIndex &rootIndex, ElisaUtils::PlayListEnqueueMode enqueueMode, ElisaUtils::PlayListEnqueueTriggerPlay triggerPlay)
{
    auto allData = DataTypes::EntryDataList{};
    auto yt = QYTMusic::getInstance();
    allData.reserve(rowCount());
    for (int rowIndex = 0, maxRowCount = rowCount(); rowIndex < maxRowCount; ++rowIndex) {
        auto currentIndex = index(rowIndex, 0, rootIndex);

        auto trackId = QFileInfo(data(currentIndex, DataTypes::ResourceRole).toUrl().path()).completeBaseName();
        auto trackUrl = QUrl(yt->getFullTrackInfo(trackId).url);
        allData.push_back({{{DataTypes::ColumnsRoles::ResourceRole, QUrl(trackUrl)},
                            {DataTypes::ColumnsRoles::ElementTypeRole, ElisaUtils::FileName},
                            {DataTypes::TitleRole, data(currentIndex, DataTypes::TitleRole).toString()},
                            {DataTypes::ImageUrlRole, QUrl(data(currentIndex, DataTypes::ImageUrlRole).toUrl())},
                            {DataTypes::HasEmbeddedCover, true},
                            {DataTypes::ArtistRole, data(currentIndex, DataTypes::ArtistRole).toString()},
                            {DataTypes::AlbumArtistRole, data(currentIndex, DataTypes::ArtistRole).toString()},
                            {DataTypes::AllArtistsRole, data(currentIndex, DataTypes::AllArtistsRole).toString()},
                            {DataTypes::RatingRole, 0},
                            {DataTypes::ElementTypeRole, ElisaUtils::Track},
                            {DataTypes::FileModificationTime, QDateTime::currentDateTime()}},
                            data(currentIndex, Qt::DisplayRole).toString(),
                            QUrl(trackUrl)});

    }
    Q_EMIT entriesToEnqueue(allData, enqueueMode, triggerPlay);
}

