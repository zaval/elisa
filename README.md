<!--
SPDX-FileCopyrightText: 2017 Matthieu Gallien <matthieu_gallien@yahoo.fr>

SPDX-License-Identifier: LGPL-3.0-or-later
-->

# Elisa + YouTube Music

Elisa music player

## Introduction

Elisa is a simple music player aiming to provide a nice experience for its users.
Elisa allows to browse music by album, artist or all tracks. The music is indexed
using either a private indexer or an indexer using Baloo. The private one can be
configured to scan music on chosen paths. The Baloo one is much faster because
Baloo is providing all needed data from its own database. You can build and play
your own playlist.

![Screenshot Elisa albums view](https://community.kde.org/images.community/3/35/Elisa_albums_view.png)

![Screenshot Elisa "Now Playing"" view](https://community.kde.org/images.community/7/75/Elisa_now_playing_view.png)

## YouTube Music support

It uses the unofficial innertube API extracted from the iPhone's mobile app.  

A new item for YouTube Music

![Screenshot Elisa Youtube Music](doc/Screenshot_20230116_001121.png)

Settings page

![Screnshot Elisa Youtube Music settings](doc/Screenshot_20230116_001625.png)

## Contributions

Contributions are very much welcome. Elisa is following a design by KDE VDG team
(https://community.kde.org/KDE_Visual_Design_Group/Music_Player).
Please use the Gitlab instance from KDE (https://invent.kde.org/multimedia/elisa/) to contribute.

